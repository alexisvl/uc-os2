/*
*********************************************************************************************************
*                                              uC/OS-II
*                                        The Real-Time Kernel
*
*                    Copyright 1992-2020 Silicon Laboratories Inc. www.silabs.com
*
*                                   Modifications for AVR-DX port:
*                                   Copyright 2021 Alexis Lockwood
*
*                                 SPDX-License-Identifier: APACHE-2.0
*
*               This software is subject to an open source license and is distributed by
*                Silicon Laboratories Inc. pursuant to the terms of the Apache License,
*                    Version 2.0 available at www.apache.org/licenses/LICENSE-2.0.
*
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*
*                                        AVR-DX    Specific code
*
* Filename : os_cpu.h
* Version  : V2.93.00
* Port by  : Alexis Lockwood - based on ATxmega128 port
*********************************************************************************************************
*/

#ifdef  OS_CPU_GLOBALS
#define OS_CPU_EXT
#else
#define OS_CPU_EXT  extern
#endif

#ifdef __cplusplus
#define OS_EXTERN_C extern "C"
#else
#define OS_EXTERN_C
#endif


/*
**********************************************************************************************************
*                                              DATA TYPES
*                                         (Compiler Specific)
**********************************************************************************************************
*/

typedef unsigned char  BOOLEAN;
typedef unsigned char  INT8U;                    /* Unsigned  8 bit quantity                            */
typedef signed   char  INT8S;                    /* Signed    8 bit quantity                            */
typedef unsigned int   INT16U;                   /* Unsigned 16 bit quantity                            */
typedef signed   int   INT16S;                   /* Signed   16 bit quantity                            */
typedef unsigned long  INT32U;                   /* Unsigned 32 bit quantity                            */
typedef signed   long  INT32S;                   /* Signed   32 bit quantity                            */
typedef float          FP32;                     /* Single precision floating point                     */

typedef unsigned char  OS_STK;                   /* Each stack entry is 8-bit wide                      */
typedef unsigned char  OS_CPU_SR;                /* Define size of CPU status register (PSW = 8 bits)   */

/*
*********************************************************************************************************
*                                             Atmel AVR
*
* Method #1:  Disable/Enable interrupts using simple instructions.  After critical section, interrupts
*             will be enabled even if they were disabled before entering the critical section.
*
* Method #2:  Disable/Enable interrupts by preserving the state of interrupts. In other words, if
*             interrupts were disabled before entering the critical section, they will be disabled when
*             leaving the critical section. The IAR compiler does not support inline assembly so I'm
*             using the _OPC() intrinsic function. Here are the instructions:
*
*             OS_ENTER_CRITICAL:
*                 ST      -Y,R16
*                 IN      R16,SREG
*                 CLI
*                 PUSH    R16
*                 LD      R16,Y+
*
*             OS_EXIT_CRITICAL:
*                 ST      -Y,R16
*                 POP     R16
*                 OUT     SREG,R16
*                 LD      R16,Y+
*
* Method #3:  Disable/Enable interrupts by preserving the state of interrupts.  Generally speaking you
*             would store the state of the interrupt disable flag in the local variable 'cpu_sr' and then
*             disable interrupts.  'cpu_sr' is allocated in all of uC/OS-II's functions that need to
*             disable interrupts.  You would restore the interrupt disable state by copying back 'cpu_sr'
*             into the CPU's status register.
*********************************************************************************************************
*/

#define  OS_CRITICAL_METHOD    3


#if OS_CRITICAL_METHOD == 1
# define OS_ENTER_CRITICAL() __asm__ volatile("cli" ::: "memory")
# define OS_EXIT_CRITICAL()  __asm__ volatile("sei" ::: "memory")
#elif OS_CRITICAL_METHOD == 2
// just pushing to the stack is pretty dodgy with a modern compiler, wth uC/OS...
# error "OS_CRITICAL_METHOD == 2 not implemented in this port."
#elif OS_CRITICAL_METHOD == 3
# define OS_ENTER_CRITICAL() __asm__ volatile("in %0, 0x3F\n\t" "cli" : "=r"(cpu_sr) :: "memory")
# define OS_EXIT_CRITICAL()  __asm__ volatile("out 0x3F, %0" :: "r"(cpu_sr) : "memory")
#endif

/*
**********************************************************************************************************
*                                          AVR Miscellaneous
**********************************************************************************************************
*/

#define  OS_STK_GROWTH      1                       /* Stack grows from HIGH to LOW memory on AVR       */

#define  OS_TASK_SW()       OSCtxSw()

/* MANAGED ISR WRAPPER
 * Use this macro instead of ISR() to generate a Managed ISR. This ISR is uC/OS-aware, and will perform
 * any stacking of task state and TCB management required to properly interact with the kernel. You should
 * not use OSIntEnter() and OSIntExit() in your ISR.
 *
 * Note that there is a performance penalty. For the fastest possible ISR, follow the uC/OS documentation
 * and do this yourself.
 *
 * This implementation biases towards lower latency getting into your interrupt, at the cost of slightly
 * slower exit.
 *
 * Implementation details:
 *
 * - Your function is renamed with a "__wrapped" suffix. The real ISR calls yours, so that control
 *   flow returns to it when finished.
 *
 * - Your function still needs to have __attribute__((signal)) so GCC generates the correct
 *   register saves. This means we need a little hack. AVR-Dx has a multilevel interrupt
 *   controller, and RETI will re-enable the current level. We need to make sure no other ISRs
 *   barge in until we're well and truly done.
 *
 *   To accomplish this, we take advantage of the fact that re-enabling interrupts, both by RETI
 *   and by SEI, is always delayed by one cycle, whereas clearing them is immediate. Therefore, the
 *   instruction immediately following the call to your function will be executed first, and this
 *   instruction is CLI, so that we can then do some cleanup with interrupts disabled.
 *
 *   After we finish this cleanup, we turn interrupts back on with SEI, then take advantage of this
 *   guarantee one more time by immediately executing a RET; this also goes in the delay slot
 *   before any switch to a new interrupt context.
 */
void OSIntEnter(void) __attribute__((used));
#define UCOS_MANAGED_ISR(name) \
OS_EXTERN_C void name (void) __attribute__((signal,naked,used)); \
void name (void) { \
    asm volatile ( \
        /* OSIntEnter() */ \
        "push r16"                                  "\n\t" \
        "in   r16, 0x3F"                            "\n\t" \
        "push r16"                                  "\n\t" \
        "lds  r16, OSRunning"                       "\n\t" \
        "cpi  r16, 1"                               "\n\t" \
        "brne 1f"                                   "\n\t" \
        "lds  r16, OSIntNesting"                    "\n\t" \
        "cpi  r16, 0xFF"                            "\n\t" \
        "breq 1f"                                   "\n\t" \
        "subi r16, 0xFF"                            "\n\t" \
        "sts  OSIntNesting, r16"                    "\n\t" \
"1:"    "pop  r16"                                  "\n\t" \
        "out  0x3F, r16"                            "\n\t" \
        "pop  r16"                                  "\n\t" \
        "call __vector" #name "__wrapped"           "\n\t" \
        "cli"                                       "\n\t" \
        /* This is pretty big, don't repeat in every ISR */ \
        "jmp  OSAvrManagedIntCtxSw"                 "\n\t" \
    );  \
} \
OS_EXTERN_C void __vector ## name ## __wrapped (void) __attribute__((signal,used)); \
void __vector ## name ## __wrapped(void)

/*
**********************************************************************************************************
*                                          GLOBAL VARIABLES
**********************************************************************************************************
*/

OS_CPU_EXT  INT16U  OSTaskStkSize;                  /* Used to set the total stack size of a task       */
OS_CPU_EXT  INT16U  OSTaskStkSizeHard;              /* Used to set the hardware stack size of a task    */

/*
**********************************************************************************************************
*                                         Function Prototypes
**********************************************************************************************************
*/

void       OSStartHighRdy(void);
void       OSCtxSw(void);
void       OSIntCtxSw(void);
